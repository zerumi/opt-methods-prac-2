package ru.ifmo.se.gitlab;

import ru.ifmo.se.gitlab.methods.ChordMethod;
import ru.ifmo.se.gitlab.methods.GoldenRatioMethod;
import ru.ifmo.se.gitlab.methods.HalfDivisionMethod;
import ru.ifmo.se.gitlab.methods.NewtonMethod;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Main {
    public static void main(String[] args) {
        BigDecimal a = BigDecimal.ONE.setScale(32, RoundingMode.HALF_UP);
        BigDecimal b = a.add(BigDecimal.ONE).setScale(32, RoundingMode.HALF_UP);
        BigDecimal epsilon = new BigDecimal("0.05").setScale(32, RoundingMode.HALF_UP);

        System.out.println("-----------HALF DIVISION-----------");
        System.out.println(new HalfDivisionMethod().findMinimum(a, b, epsilon, 32, new SourceFunction()));
        System.out.println("-----------GOLDEN RATIO------------");
        System.out.println(new GoldenRatioMethod().findMinimum(a, b, epsilon, 32, new SourceFunction()));
        System.out.println("-----------CHORD METHOD------------");
        System.out.println(new ChordMethod().findMinimum(a, b, epsilon, 32, new SourceDerivative()));
        System.out.println("-----------NEWTON METHOD-----------");
        System.out.println(new NewtonMethod().findMinimum(a, b, epsilon, 32, new SourceDerivative(), new SourceSecondDerivative()));
    }
}