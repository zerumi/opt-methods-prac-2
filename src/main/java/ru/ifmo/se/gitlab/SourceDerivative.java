package ru.ifmo.se.gitlab;

import ru.ifmo.se.gitlab.util.BigDecimalMath;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class SourceDerivative implements Function<BigDecimal, BigDecimal> {

    @Override
    public BigDecimal apply(BigDecimal x) {
        return BigDecimalMath.ln(x, x.scale()).add(x).add(x).subtract(BigDecimal.ONE).subtract(BigDecimal.ONE)
                .setScale(x.scale(), RoundingMode.HALF_UP);
    }
}
