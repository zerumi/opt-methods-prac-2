package ru.ifmo.se.gitlab;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class SourceSecondDerivative implements Function<BigDecimal, BigDecimal> {

    @Override
    public BigDecimal apply(BigDecimal x) {
        return x.add(x).add(BigDecimal.ONE)
                .divide(x, RoundingMode.HALF_UP)
                .setScale(x.scale(), RoundingMode.HALF_UP);
    }
}
