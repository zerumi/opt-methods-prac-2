package ru.ifmo.se.gitlab.methods;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class ChordMethod implements FindFunctionMinimum {

    @Override
    public BigDecimal findMinimum(BigDecimal a,
                                  BigDecimal b,
                                  BigDecimal epsilon,
                                  int scale,
                                  Function<BigDecimal, BigDecimal> derivative) {

        BigDecimal x_chord = a.subtract(derivative.apply(a)
                .divide(derivative.apply(a)
                        .subtract(derivative.apply(b))
                        , RoundingMode.HALF_UP).multiply(a.subtract(b)))
                .setScale(scale, RoundingMode.HALF_UP);

        System.out.println("a | b | eps | x_chord");

        while (derivative.apply(x_chord).compareTo(epsilon) > 0) {
            System.out.println(a + " | " + b + " | " + epsilon + " | " + x_chord);
            if (derivative.apply(x_chord).compareTo(BigDecimal.ZERO) > 0) {
                b = x_chord;
            } else {
                a = x_chord;
            }
            x_chord = a.subtract(derivative.apply(a)
                            .divide(derivative.apply(a)
                                            .subtract(derivative.apply(b))
                                    , RoundingMode.HALF_UP).multiply(a.subtract(b)))
                    .setScale(scale, RoundingMode.HALF_UP);
        }

        return x_chord;
    }
}
