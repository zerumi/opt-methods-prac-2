package ru.ifmo.se.gitlab.methods;

import java.math.BigDecimal;
import java.util.function.Function;

public interface FindFunctionMinimum {
    BigDecimal findMinimum(BigDecimal a,
                                  BigDecimal b,
                                  final BigDecimal epsilon,
                                  int scale,
                                  Function<BigDecimal, BigDecimal> f);
}
