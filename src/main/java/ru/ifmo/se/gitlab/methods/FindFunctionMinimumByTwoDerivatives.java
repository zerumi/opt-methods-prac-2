package ru.ifmo.se.gitlab.methods;

import java.math.BigDecimal;
import java.util.function.Function;

public interface FindFunctionMinimumByTwoDerivatives {
    BigDecimal findMinimum(BigDecimal a,
                           BigDecimal b,
                           BigDecimal epsilon,
                           int scale,
                           Function<BigDecimal, BigDecimal> first_derivative,
                           Function<BigDecimal, BigDecimal> second_derivative);
}
