package ru.ifmo.se.gitlab.methods;

import ru.ifmo.se.gitlab.util.BigDecimalMath;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class GoldenRatioMethod implements FindFunctionMinimum {
    public BigDecimal findMinimum(BigDecimal a,
                                  BigDecimal b,
                                  final BigDecimal epsilon,
                                  int scale,
                                  Function<BigDecimal, BigDecimal> f) {

        BigDecimal five = new BigDecimal("5").setScale(scale, RoundingMode.HALF_UP);
        BigDecimal two = new BigDecimal("2").setScale(scale, RoundingMode.HALF_UP);

        BigDecimal gr = BigDecimal.ONE.setScale(scale, RoundingMode.HALF_UP)
                .add(BigDecimalMath.sqrt(five)).divide(two, RoundingMode.HALF_UP);

        BigDecimal resgr = two.subtract(gr); // 0.382, but more accurate
        BigDecimal resgr_minus_one = BigDecimal.ONE.setScale(scale, RoundingMode.HALF_UP).subtract(resgr); // 0.618, but more accurate

        BigDecimal x1 = a.add(resgr.multiply(b.subtract(a)))
                .setScale(scale, RoundingMode.HALF_UP);
        BigDecimal x2 = a.add(resgr_minus_one
                .multiply(b.subtract(a)))
                .setScale(scale, RoundingMode.HALF_UP);

        BigDecimal y1 = f.apply(x1);
        BigDecimal y2 = f.apply(x2);

        System.out.println("a | b | eps | x1 | x2 | y1 | y2");

        do {
            System.out.println(a + " | " + b + " | " + epsilon + " | " + x1 + " | " + x2 + " | " + y1 + " | " + y2);
            if (y1.compareTo(y2) < 0) {
                b = x2;
                x2 = x1;
                y2 = y1;
                x1 = a.add(resgr.multiply(b.subtract(a)))
                        .setScale(scale, RoundingMode.HALF_UP);
                y1 = f.apply(x1);
            } else {
                a = x1;
                x1 = x2;
                y1 = y2;
                x2 = a.add(resgr_minus_one
                        .multiply(b.subtract(a)))
                        .setScale(scale, RoundingMode.HALF_UP);
                y2 = f.apply(x2);
            }
            } while (b.subtract(a).abs().compareTo(epsilon) > 0);

        return x1.add(x2).divide(two, RoundingMode.HALF_UP)
                .setScale(scale, RoundingMode.HALF_UP);
    }
}
