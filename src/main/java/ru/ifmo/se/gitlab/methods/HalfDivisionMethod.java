package ru.ifmo.se.gitlab.methods;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class HalfDivisionMethod implements FindFunctionMinimum {

    public BigDecimal findMinimum(BigDecimal a,
                                  BigDecimal b,
                                  final BigDecimal epsilon,
                                  int scale,
                                  Function<BigDecimal, BigDecimal> f) {

        final BigDecimal two = BigDecimal.ONE.add(BigDecimal.ONE).setScale(scale, RoundingMode.HALF_UP);

        System.out.println("a | b | eps | x1 | x2 | y1 | y2");

        while (b.subtract(a).compareTo(epsilon.multiply(two)) > 0) {
            BigDecimal x1 = a.add(b).subtract(epsilon).divide(two, RoundingMode.HALF_UP);
            BigDecimal x2 = a.add(b).add(epsilon).divide(two, RoundingMode.HALF_UP);

            BigDecimal y1 = f.apply(x1);
            BigDecimal y2 = f.apply(x2);

            System.out.println(a + " | " + b + " | " + epsilon + " | " + x1 + " | " + x2 + " | " + y1 + " | " + y2);

            if (y1.compareTo(y2) > 0) {
                a = x1;
            } else {
                b = x2;
            }
        }

        return a.add(b).divide(two, RoundingMode.HALF_UP);
    }
}
