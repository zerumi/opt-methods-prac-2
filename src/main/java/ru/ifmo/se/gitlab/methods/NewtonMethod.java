package ru.ifmo.se.gitlab.methods;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public class NewtonMethod implements FindFunctionMinimumByTwoDerivatives {
    @Override
    public BigDecimal findMinimum(BigDecimal a,
                                  BigDecimal b,
                                  BigDecimal epsilon,
                                  int scale,
                                  Function<BigDecimal, BigDecimal> first_derivative,
                                  Function<BigDecimal, BigDecimal> second_derivative) {

        BigDecimal two = BigDecimal.ONE.add(BigDecimal.ONE).setScale(scale, RoundingMode.HALF_UP);
        BigDecimal x = a.add(b).divide(two, RoundingMode.HALF_UP);

        System.out.println("a | b | eps | x");

        while (first_derivative.apply(x).compareTo(epsilon) > 0) {
            System.out.println(a + " | " + b + " | " + epsilon + " | " + x);
            BigDecimal new_x = x.subtract(
                    first_derivative.apply(x).divide(second_derivative.apply(x), RoundingMode.HALF_UP)
                    ).setScale(scale, RoundingMode.HALF_UP);

            while (!(new_x.compareTo(a) >= 0 && new_x.compareTo(b) <= 0)) {
                new_x = x.add(new_x).divide(two, RoundingMode.HALF_UP);
            }

            x = new_x;
        }

        return x;
    }
}
